import React from 'react';
import { getUser, removeUserSession } from './Utils/Common';
import Venues from './components/Venues/Venues'
import Header from './components/header/header'
function Dashboard(props) {
  const user = getUser();

  // handle click event of logout button
  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }

  return (
    <div>
      <Header />
      Welcome {user.firstName}!<br /><br />
      <input type="button" onClick={handleLogout} value="Logout" />
      <Venues />
    </div>
  );
}

export default Dashboard;
