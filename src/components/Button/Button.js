import React from 'react';
import classes from './Button.module.css'

const button = (props) => (
    <button 
        id = {props.id} 
        className={[classes.Button, classes[props.btnType], classes[props.color]].join(' ')}
        onClick={props.clicked}
        disabled={props.disabled ? props.disabled : false}>
            {props.children}
        </button>
)


export default button;