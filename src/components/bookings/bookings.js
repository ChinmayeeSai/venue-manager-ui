import React, { useEffect, useState } from "react";
import Grid from '../grid/Grid'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import { getToken, getUser, removeUserSession, setUserSession } from '../../Utils/Common';
import Header from '../header/header'
import Modal from '../Modal/Modal'
import BookingPopUp from '../Venues/BookingPopUp'

const Bookings = (props) => {
    const token = getToken();

    const[bookings, setBookings] = useState([])
    const [loading, setLoading] = useState(false);
    const [columns, setColumns] = useState([])
    const [error, setError] = useState(null);
    const [rows, setRows] = useState([])
    const [selectedRowParam, setSelectedRowParam] = useState({})
    const [bookingPopUp, setBookingPopUp] = useState(false)
    const [slots, setSlots] = useState([])
    const [bookedSlots, setBookedSlots] = useState([])
    useEffect(()=>{
        if(bookings.length === 0){
            axios.get(`${process.env.REACT_APP_API_URL}/booking/allbookings`,{headers: {
                Authorization: `Bearer ${token}`
            }}).then((response)=>{
                setBookings(response.data)
                setLoading(false)
                prepareGrid(response.data)
            }).catch((err)=>{
                setError(err)
                toast("Failed to load bookings..!")
                setLoading(false)
            })
        }

        if(slots.length === 0){
            axios.get(`${process.env.REACT_APP_API_URL}/venue/allslots`,{headers: {
                Authorization: `Bearer ${token}`
            }}).then((response)=>{
                setSlots(response.data)
            }).catch((err)=>{
                setError(err)
            })
        }
    },[])

    function confirmBookingHandler(params) {
        const user = getUser()
        const payload = {
            bookingDate: params.selectedDate.toISOString().split('T')[0],
            slotId: params.selectedSlot,
            userName: user.username,
            venueId: selectedRowParam.data.venueId,
            bookingId: selectedRowParam.data.bookingId
          }
        console.log(payload)
        axios.put(`${process.env.REACT_APP_API_URL}/booking/booking`, payload, {headers: {
            Authorization: `Bearer ${token}`
        }}).then(response => {
            console.log(response)
            toast("Booking confirmed..!")
        }).catch(err => {
            console.log(err)
            toast("Booking Failed..!")
            setError(err)
        })
        setBookingPopUp(false)
    }
    function cancelBookingHandler() {
        setBookingPopUp(false)
    }
    function getBookedSlots(date) {
        if(selectedRowParam.data !== undefined){
            axios.get(`${process.env.REACT_APP_API_URL}/booking/slots/${selectedRowParam.data.venueId}/${date.toISOString().split('T')[0]}`,
            {headers: {
                Authorization: `Bearer ${token}`
            }}).then(res => {
                setBookedSlots(res.data)
            }).catch(err => {
                console.log(err)
                setError(err)
            })
        }
        
    }

    function prepareGrid(data){
        const colDef = [
            {
                headerName: "ID",
                field: "bookingId"
            },
            {
                headerName: "Venue",
                field: "venueName"
            },
            {
                headerName: "Date",
                field: "bookingDate"
            },
            {
                headerName: "Status",
                field: "bookingStatus"
            },
            {
                headerName: "Action",
                field: "Action",
                cellRenderer: 'actionCellRenderer',
                cellRendererParams: {
                    cancelBooking: (val)=>{
                        cancelBooking(val)
                    },
                    rescheduleBooking: (val)=>{
                        rescheduleBooking(val)
                    }
                }
            },

        ]
        setColumns(colDef)
        const rows = data.map(booking=>{
            return {
                bookingId: booking['bookingId'],
                bookingDate: booking['bookingDate'],
                bookingStatus: booking['bookingStatus'],
                venueName: booking['venue']['venueName'],
                venueId: booking['venue']['venueId'],
                Action: booking['bookingId']
            }
        })
        
        setRows(rows)
    }
    function cellClickHandler(params) {
        setSelectedRowParam(params)
    }
    
    function cancelBooking(id) {
        axios.post(`${process.env.REACT_APP_API_URL}/booking/modify/booking?bookingId=${id}`,null,{headers: {
            Authorization: `Bearer ${token}`
        }}).then((response)=>{
            toast("Booking Cancelled..!")
        }).catch((err)=>{
            setError(err)
            toast("Failed to cancel booking..!")
        })
    }

    function rescheduleBooking(id) {
        setBookingPopUp(true)
    }
    return (
        <div>
            <ToastContainer />
            <Header />
            {loading ? `Loading...` : <Grid columnDefinition = {columns} data = {rows} cellClickHandler = {cellClickHandler}/>}
            <Modal show = {bookingPopUp} modalClosed = {()=>setBookingPopUp(false)}>
                <BookingPopUp 
                params = {selectedRowParam} 
                slots = {slots}
                cancelBookingHandler = {cancelBookingHandler}
                confirmBookingHandler = {confirmBookingHandler}
                getBookedSlots = {getBookedSlots}
                bookedSlots = {bookedSlots}/>
            </Modal>
        </div>
    )
}

export default Bookings;