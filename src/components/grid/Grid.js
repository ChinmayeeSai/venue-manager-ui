import React, { useState, useEffect } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import BtnCellRenderer from './ButtonRenderer'
import ActionCellRenderer from './ActionCellRenderer'

const Grid = (props) => {
    const [gridColumns, setColumns] = useState([])
    const [gridRows, setRows] = useState([])
    const [frameworkComponents, setFrameworkComponents] = useState({
        btnCellRenderer: BtnCellRenderer,
        actionCellRenderer: ActionCellRenderer,
      })
    useEffect(()=>{
        
        // const colDef = [
        //     {
        //         headerName: "ID",
        //         field: "venueId"
        //     },
        //     {
        //         headerName: "Name",
        //         field: "venueName"
        //     },
        //     {
        //         headerName: "Description",
        //         field: "venueDescription"
        //     },
        //     {
        //         headerName: "Action",
        //         field: "Action",
        //         cellRenderer: 'btnCellRenderer',
        //     },

        // ]
        // setColumns(colDef)
        // setRows(props.data)
    },[props.data])
    function cellClickHandler(params) {
        props.cellClickHandler(params)
    }
    return (
        <div  className="ag-theme-alpine" style={{ height: '100vh', width: '97vw' }}>
            <AgGridReact 
                rowData = {props.data}
                columnDefs = {props.columnDefinition}
                frameworkComponents = {frameworkComponents}
                onCellClicked = {cellClickHandler}
            />
        </div>
    )
}

export default Grid;