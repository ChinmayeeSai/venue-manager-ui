import React, { Component } from "react";

class BtnCellRenderer extends Component {
    constructor(props) {
      super(props);
      this.btnClickedHandler = this.btnClickedHandler.bind(this);
    }
    btnClickedHandler() {
    }
    render() {
      return (
        <button onClick={this.btnClickedHandler}>Book</button>
      )
    }
  }

  export default BtnCellRenderer