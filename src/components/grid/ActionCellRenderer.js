import React, { Component } from "react";

class ActionCellRenderer extends Component {
   
    cancelbtnClickedHandler = () => {
        this.props.cancelBooking(this.props.value)
    }
    ReschedulebtnClickedHandler = () => {
        this.props.rescheduleBooking()
    }
    render() {
      return (
          <div>
            <button onClick={this.cancelbtnClickedHandler}>Cancel</button>
            <button onClick={this.ReschedulebtnClickedHandler}>Reschedule</button>
          </div>
        
      )
    }
  }

  export default ActionCellRenderer