import React from 'react';
import {NavLink} from 'react-router-dom'
import {getUser} from '../../Utils/Common';
const Header = (props) => {
    return (
        <div className="header">
            <NavLink exact activeClassName="active" to="/">Home</NavLink>
            {getUser()===null ? <NavLink activeClassName="active" to="/login">Login</NavLink>:null}
            {getUser()!==null ? <NavLink activeClassName="active" to="/dashboard">Dashboard</NavLink>: null}
            {getUser()!==null ? <NavLink activeClassName="active" to="/mybookings">My Bookings</NavLink>: null}
          </div>
    )
}

export default Header;
