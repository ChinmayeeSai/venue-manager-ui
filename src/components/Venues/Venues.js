import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { getToken, getUser } from '../../Utils/Common';
import Grid from '../grid/Grid'
import Modal from '../Modal/Modal'
import BookingPopUp from '../Venues/BookingPopUp'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Venues = (props) => {
    
    const token = getToken();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [venues, setVenues] = useState([])
    const [slots, setSlots] = useState([])
    const [bookingPopUp, setBookingPopUp] = useState(false)
    const [selectedRowParam, setSelectedRowParam] = useState({})
    const [bookedSlots, setBookedSlots] = useState([])
    const [columns, setColumns] = useState([])
    useEffect(()=>{
        setLoading(true)
        if(venues.length === 0){
            axios.get(`${process.env.REACT_APP_API_URL}/venue/allvenues`,
            {headers: {
                Authorization: `Bearer ${token}`
            }}).then((response)=>{
                setLoading(false)
                setVenues(response.data)
                prepareGrid()
            }).catch((err)=>{
                setLoading(false)
                setError(err)
            })
        }

        if(slots.length === 0){
            axios.get(`${process.env.REACT_APP_API_URL}/venue/allslots`,{headers: {
                Authorization: `Bearer ${token}`
            }}).then((response)=>{
                setSlots(response.data)
            }).catch((err)=>{
                setError(err)
            })
        }
    },[])
    function prepareGrid() {
        const colDef = [
            {
                headerName: "ID",
                field: "venueId"
            },
            {
                headerName: "Name",
                field: "venueName"
            },
            {
                headerName: "Description",
                field: "venueDescription"
            },
            {
                headerName: "Action",
                field: "Action",
                cellRenderer: 'btnCellRenderer',
            },

        ]
        setColumns(colDef)
    }
    function cellClickHandler(params) {
        setBookingPopUp(true)
        setSelectedRowParam(params)
    }
    function confirmBookingHandler(params) {
        const user = getUser()
        const payload = {
            bookingDate: params.selectedDate.toISOString().split('T')[0],
            slotId: params.selectedSlot,
            userName: user.username,
            venueId: selectedRowParam.data.venueId
          }
        console.log(payload)
        axios.post(`${process.env.REACT_APP_API_URL}/booking/booking`, payload, {headers: {
            Authorization: `Bearer ${token}`
        }}).then(response => {
            console.log(response)
            toast("Booking confirmed..!")
        }).catch(err => {
            console.log(err)
            toast("Booking Failed..!")
            setError(err)
        })
        setBookingPopUp(false)
    }
    function cancelBookingHandler() {
        setBookingPopUp(false)
    }
    function getBookedSlots(date) {
        if(selectedRowParam.data !== undefined){
            axios.get(`${process.env.REACT_APP_API_URL}/booking/slots/${selectedRowParam.data.venueId}/${date.toISOString().split('T')[0]}`,
            {headers: {
                Authorization: `Bearer ${token}`
            }}).then(res => {
                setBookedSlots(res.data)
            }).catch(err => {
                console.log(err)
                setError(err)
            })
        }
        
    }
    return (
        <div>
            {loading ? `Loading...` : <Grid columnDefinition = {columns} data = {venues} cellClickHandler = {cellClickHandler}/>}
            <Modal show = {bookingPopUp} modalClosed = {()=>setBookingPopUp(false)}>
                <BookingPopUp 
                params = {selectedRowParam} 
                slots = {slots}
                cancelBookingHandler = {cancelBookingHandler}
                confirmBookingHandler = {confirmBookingHandler}
                getBookedSlots = {getBookedSlots}
                bookedSlots = {bookedSlots}/>
            </Modal>
            <ToastContainer />
        </div>
    )
}


export default Venues;