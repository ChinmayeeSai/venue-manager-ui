import React, {useEffect, useState} from 'react'
import SplitPane from 'react-split-pane';
import Pane from 'react-split-pane/lib/Pane';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import classes from './BookingPopUp.module.css'
import Button from '../Button/Button'

const BookingPopUp = (props) => {
    const [value, onChange] = useState(new Date());
    const [selectedSlot, setSelectedSlot] = useState("0");
    function slotSelectHandler(event) {
        console.log(event.target.id)
        setSelectedSlot(event.target.id)
    }
    useEffect(()=>{
        setSelectedSlot("0")
        //onChange(new Date())
        props.getBookedSlots(value);
    },[props.params,value])
    function onChangeHandler(param) {
        onChange(param)
        // setSelectedSlot("0")
        // props.getBookedSlots(param);
    }
    function confirmBookingHandler() {
        props.confirmBookingHandler({
            selectedSlot: selectedSlot,
            selectedDate: value
        })
        setSelectedSlot("0")
        onChange(new Date())
    }
    function cancelBookingHandler() {
        setSelectedSlot("0")
        onChange(new Date())
        props.cancelBookingHandler()
    }
    // function getColor(id) {
    //     console.log("GetColor")
    //    return props.bookedSlots.map(s=>s["slotId"]).includes(id) ? "Red" : "Green"
    // }
    return (
        <div>
            <div className = {classes.Caption}>Book Venue</div>
            <div className = {classes.DialogBox}>
                <SplitPane split="vertical">
                    <Pane className = {classes.Pane} minSize="10%" initialSize="50%">
                        <label>Pick a date:</label>
                        <Calendar 
                        onChange={onChangeHandler}
                        value={value}/>
                        <label>Selected date:{value.toLocaleDateString()}</label>
                    </Pane>
                    <Pane className = {classes.Pane} minSize="10%" initialSize="50%">
                        <label>Choose Slot:</label>
                        <ul>
                            {props.slots.map(slot=>{
                                let color = (slot.slotId == selectedSlot) ? "Blue" : props.bookedSlots.map(s=>s["slotId"]).includes(slot.slotId) ? "Red" : "Green"
                                return(
                                    <li key={slot.slotId}>
                                        <Button disabled = {color === "Red"} color = {color} id={slot.slotId} clicked = {slotSelectHandler}>{`${slot.startTime} - ${slot.endTime}`}</Button>
                                    </li>
                                )
                            })}
                        </ul>
                    </Pane>
                </SplitPane>
                <Button disabled = {selectedSlot == 0} clicked = {confirmBookingHandler}>Confirm</Button>
                <Button clicked = {cancelBookingHandler}>Cancel</Button>
            </div>
            
        </div>
    )
}

export default BookingPopUp;