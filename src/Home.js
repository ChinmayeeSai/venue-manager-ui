import React from 'react';
import Header from './components/header/header'

function Home() {
  return (
    <div>
      <Header />
      Welcome to the Home Page!
    </div>
  );
}

export default Home;
